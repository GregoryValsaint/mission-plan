<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EmployeeRepository::class)
 */
class Employee
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codeNir;

    /**
     * @ORM\ManyToMany(targetEntity=Equipe::class, inversedBy="employees")
     */
    private $teams;

    /**
     * @ORM\OneToMany(targetEntity=Equipe::class, mappedBy="director")
     */
    private $directedTeams;

    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->directedTeams = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getCodeNir(): ?string
    {
        return $this->codeNir;
    }

    public function setCodeNir(string $codeNir): self
    {
        $this->codeNir = $codeNir;

        return $this;
    }

    /**
     * @return Collection|Equipe[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Equipe $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
        }

        return $this;
    }

    public function removeTeam(Equipe $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
        }

        return $this;
    }

    /**
     * @return Collection|Equipe[]
     */
    public function getDirectedTeams(): Collection
    {
        return $this->directedTeams;
    }

    public function addDirectedTeam(Equipe $directedTeam): self
    {
        if (!$this->directedTeams->contains($directedTeam)) {
            $this->directedTeams[] = $directedTeam;
            $directedTeam->setDirector($this);
        }

        return $this;
    }

    public function removeDirectedTeam(Equipe $directedTeam): self
    {
        if ($this->directedTeams->contains($directedTeam)) {
            $this->directedTeams->removeElement($directedTeam);
            // set the owning side to null (unless already changed)
            if ($directedTeam->getDirector() === $this) {
                $directedTeam->setDirector(null);
            }
        }

        return $this;
    }
    public function __toString() {
        return $this->nom.'-'.$this->prenom;
    }
}
