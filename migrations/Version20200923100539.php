<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200923100539 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(50) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(100) NOT NULL, prenom VARCHAR(100) NOT NULL, code_nir VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employee_equipe (employee_id INT NOT NULL, equipe_id INT NOT NULL, INDEX IDX_D787E808C03F15C (employee_id), INDEX IDX_D787E806D861B89 (equipe_id), PRIMARY KEY(employee_id, equipe_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE equipe (id INT AUTO_INCREMENT NOT NULL, director_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_2449BA15899FB366 (director_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE mission (id INT AUTO_INCREMENT NOT NULL, team_id INT DEFAULT NULL, client_id INT DEFAULT NULL, nom VARCHAR(50) NOT NULL, description LONGTEXT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, mission_ended TINYINT(1) NOT NULL, INDEX IDX_9067F23C296CD8AE (team_id), INDEX IDX_9067F23C19EB6921 (client_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE employee_equipe ADD CONSTRAINT FK_D787E808C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE employee_equipe ADD CONSTRAINT FK_D787E806D861B89 FOREIGN KEY (equipe_id) REFERENCES equipe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE equipe ADD CONSTRAINT FK_2449BA15899FB366 FOREIGN KEY (director_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23C296CD8AE FOREIGN KEY (team_id) REFERENCES equipe (id)');
        $this->addSql('ALTER TABLE mission ADD CONSTRAINT FK_9067F23C19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23C19EB6921');
        $this->addSql('ALTER TABLE employee_equipe DROP FOREIGN KEY FK_D787E808C03F15C');
        $this->addSql('ALTER TABLE equipe DROP FOREIGN KEY FK_2449BA15899FB366');
        $this->addSql('ALTER TABLE employee_equipe DROP FOREIGN KEY FK_D787E806D861B89');
        $this->addSql('ALTER TABLE mission DROP FOREIGN KEY FK_9067F23C296CD8AE');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE employee_equipe');
        $this->addSql('DROP TABLE equipe');
        $this->addSql('DROP TABLE mission');
    }
}
